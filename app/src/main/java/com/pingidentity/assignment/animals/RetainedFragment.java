package com.pingidentity.assignment.animals;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;

/**
 * This Fragment manages a single background task and retains
 * itself across configuration changes.
 * Created by Mishustin Evgeniy on 09/05/2016.
 */
public class RetainedFragment extends Fragment {

    private ArrayList<AnimalModel> list;

    private TaskCallbacks mCallbacks;

    /**
     * Callback interface through which the fragment will report the
     * task's complete and results back to the Activity.
     */
    interface TaskCallbacks {
        void loadComplete(ArrayList<AnimalModel> list);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        new GetJsonTask().execute("https://s3.amazonaws.com/accells_res/mobile_test/animals.json");
    }

    /**
     * Hold a reference to the parent Activity so we can report the
     * task's current progress and results. The Android framework
     * will pass us a reference to the newly created Activity after
     * each configuration change.
     */
    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mCallbacks = (TaskCallbacks) activity;
    }

    /**
     * Set the callback to null so we don't accidentally leak the
     * Activity instance.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    public ArrayList<AnimalModel> getList(){
        return list;
    }

    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        if (inputStream != null) {
            StringBuilder sb = new StringBuilder();
            String line;

            try {
                BufferedReader r1 = new BufferedReader(new InputStreamReader(
                        inputStream, "UTF-8"));
                while ((line = r1.readLine()) != null) {
                    sb.append(line).append("\n");
                }
            } finally {
                inputStream.close();
            }
            return sb.toString();
        } else {
            return "";
        }
    }

    class GetJsonTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            URL url;
            try {
                url = new URL(strings[0]);
                System.out.println(url);
                URLConnection conn = url.openConnection();

                HttpURLConnection httpConn = (HttpURLConnection) conn;
                httpConn.setAllowUserInteraction(false);
                httpConn.setInstanceFollowRedirects(true);
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                InputStream is = httpConn.getInputStream();
                return convertInputStreamToString(is);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            list = createSortedListOfModels(s);
            mCallbacks.loadComplete(list);
        }
    }

    /*
     * Creates a sorted-by-name arrayList of AnimalModel objects
     * from parsing JSON Object received from server
     */
    private ArrayList<AnimalModel> createSortedListOfModels(String s) {
        try {
            if(s==null) return null;
            JSONArray jsonAnimals = new JSONObject(s).getJSONArray("animals");
            ArrayList<AnimalModel> animals = new ArrayList<>();
            for (int i = 0; i < jsonAnimals.length(); i++) {
                JSONObject animalJson = jsonAnimals.getJSONObject(i);
                String animalName = animalJson.names().get(0).toString();
                AnimalModel animal = new AnimalModel(animalName,
                        animalJson.getJSONObject(animalName).getString("description"),
                        animalJson.getJSONObject(animalName).getString("image"));
                animals.add(animal);
            }
            Collections.sort(animals); //will sort by name
            return animals;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


}
