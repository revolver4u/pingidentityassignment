package com.pingidentity.assignment.animals;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * This class adapts an Array of AnimalModel to its Grid-based view
 * Created by Mishustin Evgeniy on 09/05/2016.
 */
public class GridAdapter extends RecyclerView.Adapter<GridAdapter.GridHolder> {


    private ArrayList<AnimalModel> list;
    protected Context context;
    public GridAdapter(Context context, ArrayList<AnimalModel> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public GridHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item, parent, false);

        return new GridHolder(view);
    }

    @Override
    public int getItemCount() {
        //for possible empty initialization
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    @Override
    public void onBindViewHolder(GridHolder holder, int position) {
        final AnimalModel animal = list.get(position);

        holder.image.setTag(holder);

        holder.text.setText(animal.getName());

        ImageView imageView = holder.image;
        ProgressBar progress = holder.progress;
        ImageLoader.getINSTANCE().DisplayImage(animal.getImagePath(), imageView, progress);
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("com.pingidentity.animal");
                Bundle b = new Bundle();
                b.putString("path", animal.getImagePath());
                b.putString("name", animal.getName());
                b.putString("desc", animal.getDescription());
                intent.putExtras(b);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        });
    }

    //reloads adapter after getting data from background
    public void setData(ArrayList<AnimalModel> data) {
        this.list = data;
        notifyDataSetChanged();
    }

    public static class GridHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public TextView text;
        public ProgressBar progress;

        public GridHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            text = (TextView) itemView.findViewById(R.id.text);
            progress = (ProgressBar)itemView.findViewById(R.id.progress);

        }


    }


}
