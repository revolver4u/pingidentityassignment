package com.pingidentity.assignment.animals;

/**
 * Class that describes an Animal Object
 * implements Comparable since we want to sort the appearance by name
 * Created by Mishustin Evgeniy on 09/05/2016.
 */
public class AnimalModel implements Comparable<AnimalModel>{

    private String name;
    private String description;
    private String imagePath;

    public AnimalModel(String n, String d, String i){
        this.name = n;
        this.description = d;
        this.imagePath = i;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImagePath() {
        return imagePath;
    }

    @Override
    public int compareTo(AnimalModel animalModel) {
        return this.name.compareToIgnoreCase(animalModel.getName());
    }
}
