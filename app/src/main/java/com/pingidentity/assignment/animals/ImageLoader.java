package com.pingidentity.assignment.animals;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Singleton image Loader that will load images in background using
 * several threads and update the UI
 * Created by Evgeniy Mishustin on 10/05/2016.
 */
public class ImageLoader {

    MemoryCache memoryCache = new MemoryCache();


    private static ImageLoader INSTANCE;
    public static synchronized ImageLoader getINSTANCE(){
        if(INSTANCE==null){
            INSTANCE = new ImageLoader();
        }
        return INSTANCE;
    }
    //Create Map (collection) to store image and image url in key value pair
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
    ExecutorService executorService;

    //handler to display images in UI thread
    Handler handler = new Handler();

    private ImageLoader() {
        // Creates a thread pool that reuses a fixed number of
        // threads operating off a shared unbounded queue.
        executorService = Executors.newFixedThreadPool(5);


    }

    // default image show in list (Before online image download)
    final int stub_id = R.drawable.rect;

    public void DisplayImage(String url, ImageView imageView, ProgressBar progressBar) {
        //Store image and url in Map
        imageViews.put(imageView, url);

        //Check image is stored in MemoryCache Map or not (see MemoryCache.java)
        Bitmap bitmap = memoryCache.get(url);

        if (bitmap != null) {
            // if image is stored in MemoryCache Map then
            // Show image in listview row
            imageView.setImageBitmap(bitmap);
            progressBar.setVisibility(View.INVISIBLE);
        } else {
            progressBar.setVisibility(View.VISIBLE);
            //queue Photo to download from url
            queuePhoto(url, imageView, progressBar);

            //Before downloading image show default image
            imageView.setImageResource(stub_id);
        }
    }

    private void queuePhoto(String url, ImageView imageView, ProgressBar bar) {
        // Store image and url in bitmapToLoad object
        bitmapToLoad p = new bitmapToLoad(url, imageView, bar);
        // pass PhotoToLoad object to PhotosLoader runnable class
        // and submit PhotosLoader runnable to executers to run runnable
        // Submits a PhotosLoader runnable task for execution
        executorService.submit(new BitmapLoader(p));
    }

    //Task for the queue
    private class bitmapToLoad {
        public String url;
        public ImageView imageView;
        public ProgressBar progressBar;
        public bitmapToLoad(String u, ImageView i, ProgressBar bar) {
            url = u;
            imageView = i;
            progressBar = bar;
        }
    }

    class BitmapLoader implements Runnable {
        bitmapToLoad bitmapToLoad;

        BitmapLoader(bitmapToLoad bitmapToLoad) {
            this.bitmapToLoad = bitmapToLoad;
        }

        @Override
        public void run() {
            try {
                //Check if image already downloaded
                if (imageViewReused(bitmapToLoad))
                    return;
                // download image from web url
                Bitmap bmp = getBitmap(bitmapToLoad.url);

                // set image data in Memory Cache
                memoryCache.put(bitmapToLoad.url, bmp);

                if (imageViewReused(bitmapToLoad))
                    return;

                // Get bitmap to display
                BitmapDisplayer bd = new BitmapDisplayer(bmp, bitmapToLoad);

                // Causes the Runnable bd (BitmapDisplayer) to be added to the message queue.
                // The runnable will be run on the thread to which this handler is attached.
                // BitmapDisplayer run method will call
                handler.post(bd);

            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    private Bitmap getBitmap(String url) {

        // Download image file from web
        try {

            Bitmap bitmap;
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            //get only dimensions and not pixel data at first
            BitmapFactory.decodeStream(is, null, options);
            is.close();     //close InputStream as it can't be reused
            conn.disconnect();

            // Calculate inSampleSize
            options.inSampleSize = Utils.calculateInSampleSize(options, 240, 240);
            options.inJustDecodeBounds = false; //now bring the pixel data
            conn = (HttpURLConnection) imageUrl.openConnection();
            is= conn.getInputStream();

            bitmap = BitmapFactory.decodeStream(is, null, options);
            conn.disconnect();

            return bitmap;

        } catch (Throwable ex) {
            ex.printStackTrace();
            if (ex instanceof OutOfMemoryError)
                memoryCache.clear();
            return null;
        }
    }



    boolean imageViewReused(bitmapToLoad bitmapToLoad) {
        String tag = imageViews.get(bitmapToLoad.imageView);
        //Check url is already exist in imageViews MAP
        return  !((tag != null) && bitmapToLoad.url.equals(tag));
    }

    //Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        bitmapToLoad bitmapToLoad;

        public BitmapDisplayer(Bitmap b, bitmapToLoad p) {
            bitmap = b;
            bitmapToLoad = p;
        }

        public void run() {
            if (imageViewReused(bitmapToLoad))
                return;

            // Show bitmap on UI
            if (bitmap != null) {
                bitmapToLoad.imageView.setImageBitmap(bitmap);
                bitmapToLoad.progressBar.setVisibility(View.INVISIBLE);
            }
            else {
                bitmapToLoad.imageView.setImageResource(stub_id);
                bitmapToLoad.progressBar.setVisibility(View.VISIBLE);
            }
        }
    }


}
