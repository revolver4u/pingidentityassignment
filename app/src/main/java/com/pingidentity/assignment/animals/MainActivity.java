package com.pingidentity.assignment.animals;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements RetainedFragment.TaskCallbacks {

    private static final String RETAINED_FRAGMENT_TAG = "retained_tag";
    private RetainedFragment retainedFragment;
    private RecyclerView gridView;
    private GridAdapter adapter;
    private ArrayList<AnimalModel> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        //set action bar title centered
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_custom_view);
        TextView title = (TextView) findViewById(R.id.imager);
        title.setText(getString(R.string.imager));

        FragmentManager fm = getSupportFragmentManager();
        retainedFragment = (RetainedFragment) fm.findFragmentByTag(RETAINED_FRAGMENT_TAG);

        // If the Fragment is non-null, then it is currently being
        // retained across a configuration change.
        if (retainedFragment == null) {
            retainedFragment = new RetainedFragment();
            fm.beginTransaction().add(retainedFragment, RETAINED_FRAGMENT_TAG).commit();
        } else {
            list = retainedFragment.getList();
        }

        gridView = (RecyclerView) findViewById(R.id.recycler_view);
        gridView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        adapter = new GridAdapter(this, list);
        gridView.setAdapter(adapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(onItemClick, new IntentFilter("com.pingidentity.animal"));
    }

    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(onItemClick);
    }

    //on the first load we need to get the list from background thread
    @Override
    public void loadComplete(ArrayList<AnimalModel> list) {
        this.list = list;
        adapter.setData(list);
    }

    private BroadcastReceiver onItemClick = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            startActivity(intent);
        }
    };
}
