package com.pingidentity.assignment.animals;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Details Activity. Shows description image and name
 * All the background work handled by ImageLoader thus no need to handle configuration changes
 * or implement retained fragments
 * Created by Evgeniy Mishustin on 10/05/2016.
 */
public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_layout);
        //set action bar title centered
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_custom_view);
        TextView title = (TextView) findViewById(R.id.imager);
        title.setText(getString(R.string.description));
        //init views
        ImageView image = (ImageView) findViewById(R.id.image_detail);
        TextView nameT = (TextView) findViewById(R.id.name_detail);
        TextView descriptionT = (TextView) findViewById(R.id.desc_detail);
        ProgressBar progress = (ProgressBar) findViewById(R.id.progress_details);
        Bundle b = getIntent().getExtras();
        //parse extras
        String path = b.getString("path");
        String name = b.getString("name");
        String description = b.getString("desc");
        //set Strings
        if (nameT != null) {
            nameT.setText(name);
        }
        if (descriptionT != null) {
            descriptionT.setText(description);
        }

        //Try to get image from cache, otherwise load it
        ImageLoader.getINSTANCE().DisplayImage(path, image, progress);
    }


}
